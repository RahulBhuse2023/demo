const express = require('express');
const app = express();
require('dotenv').config()
app.get('/',(req,res)=>{
    res.json({msg:"home"});
})
app.get('/about',(req,res)=>{
    res.json({msg:"about"});
})
app.get('/me',(req,res)=>{
    res.json({msg:"me"});
})
app.listen(process.env.PORT,()=>{
    console.log("server runs on port " + process.env.PORT);
})